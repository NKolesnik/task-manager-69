package ru.t1consulting.nkolesnik.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.data.DataJsonFasterXmlSaveRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;

@Component
public final class DataJsonFasterXmlSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-fasterxml-json";

    @NotNull
    public static final String DESCRIPTION = "Save data to JSON file using FasterXML.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonFasterXmlSaveListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonFasterXmlSaveRequest(getToken()));
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
