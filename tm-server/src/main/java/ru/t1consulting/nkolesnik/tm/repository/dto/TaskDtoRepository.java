package ru.t1consulting.nkolesnik.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDto;

import java.util.List;

@Repository
public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> {

    @Nullable
    @Query("SELECT t FROM TaskDto t WHERE t.id=:id AND t.userId=:userId")
    TaskDto findById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @NotNull
    @Query("SELECT t FROM TaskDto t WHERE t.userId=:userId")
    List<TaskDto> findAll(
            @Nullable @Param("userId") final String userId
    );

    @NotNull
    @Query("SELECT t FROM TaskDto t WHERE t.userId=:userId ORDER BY :sortOrder")
    List<TaskDto> findAll(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("sortOrder") final String sortOrder
    );

    @NotNull
    @Query("SELECT t FROM TaskDto t WHERE t.userId=:userId AND t.projectId=:projectId")
    List<TaskDto> findAllByProjectId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId
    );

    @Query("SELECT COUNT(1) = 1 FROM TaskDto t WHERE t.id=:id AND t.userId=:userId")
    boolean existsById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @Query("SELECT COUNT(t) FROM TaskDto t WHERE t.userId=:userId")
    long getSize(
            @Nullable @Param("userId") final String userId
    );

    @Modifying
    @Query("DELETE FROM TaskDto t WHERE t.userId=:userId AND t.id=:id")
    void removeById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @Modifying
    @Query("DELETE FROM TaskDto t WHERE t.projectId=:projectId")
    void removeByProjectId(
            @Nullable @Param("projectId") final String projectId
    );

    @Modifying
    @Query("DELETE FROM TaskDto t WHERE t.projectId=:projectId AND t.userId=:userId")
    void removeByProjectId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId
    );

    @Modifying
    @Query("DELETE FROM TaskDto t WHERE t.userId=:userId")
    void clear(
            @Nullable @Param("userId") final String userId
    );
}
