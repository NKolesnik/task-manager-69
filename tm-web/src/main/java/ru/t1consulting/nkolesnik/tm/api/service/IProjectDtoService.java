package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import java.util.Collection;
import java.util.List;

public interface IProjectDtoService {

    void create(@NotNull final String userId);

    boolean existsById(@NotNull final String userId, @NotNull String id);

    @Nullable ProjectDto findById(@NotNull final String userId, @NotNull String id);

    @Nullable Collection<ProjectDto> findAll(@NotNull final String userId);

    long count(@NotNull final String userId);

    void save(@NotNull final String userId, @NotNull ProjectDto project);

    void deleteById(@NotNull final String userId, @NotNull String id);

    void delete(@NotNull final String userId, @NotNull ProjectDto project);

    void deleteAll(@NotNull final String userId, @NotNull List<ProjectDto> projectList);

    void clear(@NotNull final String userId);

}
