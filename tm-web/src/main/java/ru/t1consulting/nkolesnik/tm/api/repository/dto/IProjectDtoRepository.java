package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import java.util.Collection;

@Repository
public interface IProjectDtoRepository extends JpaRepository<ProjectDto, String> {

    boolean existsByUserIdAndId(String userId, String id);

    ProjectDto findByUserIdAndId(String userId, String id);

    Collection<ProjectDto> findAllByUserId(String userId);

    long countByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    void deleteByUserId(String userId);

}
