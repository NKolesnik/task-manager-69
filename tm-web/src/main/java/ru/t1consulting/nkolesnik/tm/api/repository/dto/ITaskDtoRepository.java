package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import java.util.Collection;

@Repository
public interface ITaskDtoRepository extends JpaRepository<TaskDto, String> {

    boolean existsByUserIdAndId(String userId, String id);

    TaskDto findByUserIdAndId(String userId, String id);

    Collection<TaskDto> findAllByUserId(String userId);

    long countByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    void deleteByUserId(String userId);

}
