package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import java.util.Collection;
import java.util.List;

@Service
public class ProjectDtoService implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    public void add(@NotNull final String userId, @NotNull final ProjectDto project) {
        project.setUserId(userId);
        projectRepository.saveAndFlush(project);
    }

    @Override
    public void create(@NotNull final String userId) {
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName("New Project " + System.currentTimeMillis());
        projectRepository.saveAndFlush(project);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return projectRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public ProjectDto findById(@NotNull final String userId, @NotNull final String id) {
        return projectRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public Collection<ProjectDto> findAll(@NotNull final String userId) {
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public long count(@NotNull final String userId) {
        return projectRepository.countByUserId(userId);
    }

    @Override
    public void save(@NotNull final String userId, @NotNull final ProjectDto project) {
        project.setUserId(userId);
        projectRepository.saveAndFlush(project);
    }

    @Override
    public void deleteById(@NotNull final String userId, @NotNull final String id) {
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void delete(@NotNull final String userId, @NotNull final ProjectDto project) {
        deleteById(userId, project.getId());
    }

    @Override
    public void deleteAll(@NotNull final String userId, @NotNull final List<ProjectDto> projectList) {
        projectList.stream().forEach(project -> deleteById(userId, project.getId()));
    }

    @Override
    public void clear(@NotNull final String userId) {
        projectRepository.deleteByUserId(userId);
    }

}
