package ru.t1consulting.nkolesnik.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {


    @WebMethod
    @PutMapping("/create")
    void create();

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDto findById(@PathVariable("id") String id);

    @WebMethod
    @GetMapping("/findAll")
    Collection<ProjectDto> findAll();

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "project")
            @RequestBody ProjectDto project
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project")
            @RequestBody ProjectDto project
    );

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "projects")
            @RequestBody List<ProjectDto> projects
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();
}
