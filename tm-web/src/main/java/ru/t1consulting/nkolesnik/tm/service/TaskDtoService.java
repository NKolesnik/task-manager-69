package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import java.util.Collection;
import java.util.List;

@Service
public class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    public void add(@NotNull final String userId, @NotNull final TaskDto task) {
        task.setUserId(userId);
        taskRepository.saveAndFlush(task);
    }

    @Override
    public void create(@NotNull final String userId) {
        @NotNull final TaskDto task = new TaskDto();
        task.setUserId(userId);
        task.setName("New Task " + System.currentTimeMillis());
        taskRepository.saveAndFlush(task);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return taskRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public TaskDto findById(@NotNull final String userId, @NotNull final String id) {
        return taskRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public Collection<TaskDto> findAll(@NotNull final String userId) {
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public long count(@NotNull final String userId) {
        return taskRepository.countByUserId(userId);
    }

    @Override
    public void save(@NotNull final String userId, @NotNull final TaskDto task) {
        task.setUserId(userId);
        taskRepository.saveAndFlush(task);
    }

    @Override
    public void deleteById(@NotNull final String userId, @NotNull final String id) {
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void delete(@NotNull final String userId, @NotNull final TaskDto task) {
        deleteById(userId, task.getId());
    }

    @Override
    public void deleteAll(@NotNull final String userId, @NotNull final List<TaskDto> taskList) {
        taskList.stream().forEach(task -> deleteById(userId, task.getId()));
    }

    @Override
    public void clear(@NotNull final String userId) {
        taskRepository.deleteByUserId(userId);
    }

}
