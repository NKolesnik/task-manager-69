package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1consulting.nkolesnik.tm.api.endpoint.ITaskEndpoint;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;
import ru.t1consulting.nkolesnik.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1consulting.nkolesnik.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @Autowired
    ITaskDtoService taskService;

    @Override
    @WebMethod
    @PutMapping("/create")
    public void create() {
        taskService.create(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskService.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDto findById(@NotNull @PathVariable("id") final String id) {
        return taskService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<TaskDto> findAll() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskService.count(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final TaskDto task) {
        taskService.save(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody final TaskDto task
    ) {
        taskService.delete(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id) {
        taskService.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody final List<TaskDto> tasks) {
        taskService.deleteAll(UserUtil.getUserId(), tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        taskService.clear(UserUtil.getUserId());
    }

}
