package ru.t1consulting.nkolesnik.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @WebMethod
    @PutMapping("/create")
    void create();

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDto findById(@PathVariable("id") String id);

    @WebMethod
    @GetMapping("/findAll")
    Collection<TaskDto> findAll();

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "task")
            @RequestBody TaskDto task
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task")
            @RequestBody TaskDto task
    );

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "tasks")
            @RequestBody List<TaskDto> tasks
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

}
