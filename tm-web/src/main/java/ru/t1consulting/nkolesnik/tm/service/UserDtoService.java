package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IUserDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.enumerated.RoleType;
import ru.t1consulting.nkolesnik.tm.exception.user.UserEmptyLoginException;
import ru.t1consulting.nkolesnik.tm.exception.user.UserEmptyPasswordException;
import ru.t1consulting.nkolesnik.tm.exception.user.UserEmptyRoleException;
import ru.t1consulting.nkolesnik.tm.exception.user.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.model.dto.RoleDto;
import ru.t1consulting.nkolesnik.tm.model.dto.UserDto;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class UserDtoService implements IUserDtoService {

    @NotNull
    @Autowired
    private IUserDtoRepository repository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("test", "test", RoleType.USUAL);
    }

    private void initUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new UserEmptyLoginException();
        if (password == null || password.isEmpty()) throw new UserEmptyPasswordException();
        if (roleType == null) throw new UserEmptyRoleException();
        @Nullable final UserDto user = repository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Override
    @Transactional
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new UserEmptyLoginException();
        if (password == null || password.isEmpty()) throw new UserEmptyPasswordException();
        if (roleType == null) throw new UserEmptyRoleException();
        @NotNull final UserDto user = new UserDto(login, passwordEncoder.encode(password));
        @NotNull final RoleDto role = new RoleDto(user, roleType);
        user.setRoles(Collections.singletonList(role));
        repository.save(user);
    }

    @Override
    @NotNull
    public UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new UserEmptyLoginException();
        @Nullable final UserDto user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

}
