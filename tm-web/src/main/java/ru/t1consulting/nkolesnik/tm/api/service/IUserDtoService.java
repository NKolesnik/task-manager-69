package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.enumerated.RoleType;
import ru.t1consulting.nkolesnik.tm.model.dto.UserDto;

public interface IUserDtoService {

    @Transactional
    void createUser(
            @Nullable String login,
            @Nullable String password,
            @Nullable RoleType roleType
    );

    @NotNull UserDto findByLogin(@Nullable String login);

}
