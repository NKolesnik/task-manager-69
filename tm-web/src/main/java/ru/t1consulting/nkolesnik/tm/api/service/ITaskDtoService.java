package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import java.util.Collection;
import java.util.List;

public interface ITaskDtoService {

    void create(@NotNull final String userId);

    boolean existsById(@NotNull final String userId, @NotNull String id);

    @Nullable TaskDto findById(@NotNull final String userId, @NotNull String id);

    @Nullable Collection<TaskDto> findAll(@NotNull final String userId);

    long count(@NotNull final String userId);

    void save(@NotNull final String userId, @NotNull TaskDto task);

    void deleteById(@NotNull final String userId, @NotNull String id);

    void delete(@NotNull final String userId, @NotNull TaskDto task);

    void deleteAll(@NotNull final String userId, @NotNull List<TaskDto> taskList);

    void clear(@NotNull final String userId);

}
