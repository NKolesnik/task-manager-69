package ru.t1consulting.nkolesnik.tm.exception.user;

public final class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}