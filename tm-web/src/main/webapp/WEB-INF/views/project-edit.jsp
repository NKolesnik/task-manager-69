<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Edit project</h1>

<form:form action="/project/edit/${project.id}/" method="POST" modelAttribute="project">

    <form:input type="hidden" path="id"/>
    <form:input type="hidden" path="created"/>

    <p>
        <div>Name:</div>
        <div>
            <form:input type="text" path="name"/>
        </div>
    </p>

    <p>
        <div>Description:</div>
        <div>
            <form:input type="text" path="description"/>
        </div>
    </p>

    <p>
        <div>Status:</div>
        <div>
           <form:select path="status">
               <form:option value ="${null}" label="--- // ---" />
               <form:options items ="${statuses}" itemLabel="displayName" />
           </form:select>
        </div>
    </p>

    <p>
        <div>Date begin:</div>
        <div>
            <form:input type="date" path="dateBegin"/>
        </div>
    </p>

    <p>
        <div>Date end:</div>
        <div>
            <form:input type="date" path="dateEnd"/>
        </div>
    </p>

    <button type="submit">Save project</button>
</form:form>

<jsp:include page="../include/_footer.jsp" />
